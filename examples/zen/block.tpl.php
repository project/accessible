<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>" 
  <?php if ($block->accessibility->aria_attributes) {print 'role="'. $block->accessibility->aria_attributes .'"' ;} ?> >
<?php if (!empty($block->subject) && $block->module != 'menu'): ?>
  <h2 <?php if ($block->offscreen) { print 'class="element-invisible"';}?>><?php print $block->subject ?></h2>
<?php endif;?>

  <div class="content"><?php if ($block->module == 'menu' && !empty($block->subject)): ?>
  <h2 <?php if ($block->offscreen) { print 'class="element-invisible"';}?> ><?php print $block->subject ?></h2>
<?php endif;?><?php print $block->content ?></div>
</div>
