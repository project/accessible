<?php

/**
 * @file
 * The accessible module admininstration interface.
 *
 */ 

/**
 * Admin form for Accessible API  module.
 *
 * @param $form_state
 *   A form state array.
 * @param $sid
 *   A LDAP server ID.
 *
 * @return
 *   The form structure.
 */
function accessible_api_admin_edit($form, &$form_state, $op) {
//print "<pre>";print_r($op); die;
  if ($op == "edit" || $op == NULL) {
    
    
    $form['accessible_api']['accessible_api_guideline_types_preample'] = array(
        '#markup' =>  t('Many of the options below are simply preferences.
          Modules and themes within Drupal may or may not follow them.'),
      );
    
    $options = array();
    foreach (accessible_api_data('guideline_types') as $guideline_key => $guideline_data) {
      $desc = ($guideline_data['description']) ? '<div class="description">'. t($guideline_data['description']) .'</div>' : NULL;
      $options[$guideline_key] = $guideline_data['name'] . $desc;
    }
    
    $form['accessible_api']['accessible_api_guideline_types'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Guidelines to be Followed',
        '#size' => 10,
        '#options' => $options,
        '#description' => t('Which accessibility guidelines do you want to guide your
          content editing?'),
        '#default_value' =>  variable_get('accessible_api_guideline_types',array()),
      );

    $options = accessible_api_functionality_options();
    $default_options = array_keys(accessible_api_data('functionality'));
    $form['accessible_api']['accessible_api_functionality'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Functionality Enabled',
        '#size' => 10,
        '#options' => $options,
        '#description' => t('What functionality related to accessibility do you want on your
          site?  Checking something below does not mean it will happen, it just means that
          your preferences will be available to other modules and themes if they care
          and are able to implement them.'),
        '#default_value' =>  variable_get('accessible_api_functionality',array())
      );

    $form['accessible_api']['accessible_prefs_in_theme'] = array(
        '#type' => 'checkbox',
        '#title' => 'Make these preferences available in theme layer as "accessible" variable.',
        '#size' => 10,
        '#description' => t('By allowing these settting to be available in the theme
          layer, some themes can change the way their html markup is done to
          meet your preferences.'),
        '#default_value' =>  variable_get('accessible_prefs_in_theme', 1 ),
      );

 
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );

    return $form;
  }
  else {
    
    drupal_goto(ACCESSIBLE_API_MENU_BASE_PATH);
  }
}

/**
 * Submit hook for the admin settings form.
 */
function accessible_api_admin_edit_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  if ($values['op'] == t('Reset') && $values['confirm'] == 1) {
      variable_set('accessible_api_guideline_types', array());
      variable_set('accessible_api_functionality', array());
      variable_set('accessible_prefs_in_theme', 1);
      drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    $values = $form_state['values'];
   // dpm('values'); dpm($values);
    variable_set('accessible_api_guideline_types',   array_filter($values['accessible_api_guideline_types']));
    variable_set('accessible_api_functionality',  array_filter($values['accessible_api_functionality']));
    variable_set('accessible_prefs_in_theme', $values['accessible_prefs_in_theme']);
      drupal_set_message(t('The configuration options have been saved.'));
  }

  $form_state['redirect'] = ACCESSIBLE_API_MENU_BASE_PATH;
}

