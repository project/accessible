<?php

/**
 * @file
 * 
 */

function accessible_api_landing() {
  return "accessibility module links placeholder.";
//  $management = menu_load('management');
 // print "<pre>";
  //print_r($management);
  
  // return accessible_api_landing_page(ACCESSIBLE_MENU_BASE_PATH);
}



/**
 * Provide the administration overview page.
 * this is a clone of the code that produces the admin page.
 */
function accessible_api_landing_page($base_path) {

  $blocks = array();

  $result = db_query("
    SELECT m.*, ml.*
    FROM {menu_links} ml
    INNER JOIN {menu_router} m ON ml.router_path = m.path
    WHERE menu_name = 'management'
      AND ml.link_path like '". $base_path ."%'
      AND hidden = 0")->fetchAll(PDO::FETCH_ASSOC);
  
 // array(), array('fetch' => PDO::FETCH_ASSOC));
 // print "<pre>"; print_r($result); 
  foreach ($result as $item) {
    if ($item['path'] == $base_path) continue; // skip landing page item
   print "<pre>"; print_r($item);
    _menu_link_translate($item);

    $block = $item;
    $block['content'] = '';
    $block['show'] = TRUE;
    $block['content'] .= theme('admin_block_content', system_admin_menu_block($item));
    // Prepare for sorting as in function _menu_tree_check_access().
    // The weight is offset so it is always positive, with a uniform 5-digits.
    $blocks[(50000 + $item['weight']) . ' ' . $item['title'] . ' ' . $item['mlid']] = $block;
  }
  
 // die;
  if ($blocks) {
    ksort($blocks);
    return theme('admin_page', $blocks);
  }
  else {
    return t('You do not have any accessibility related items.');
  }
  
}
