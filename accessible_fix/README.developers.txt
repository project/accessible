
Accessible Fix readme for developers.

I.  ARCHITECTURE RATIONALE

The module is designed for short term, small fixes so code is in many small, modular files
to allow for simple patching.  Its also designed for code execution and admin options to be limited to only enabled modules.

I.  OVERVIEW

A. Accessible Fix leverages the preferences provided by Accessible API when possible.
B. It is meant as a stop gap measure until issues are resolved in core and contrib modules.
C. It genertally intevenes in other modules through the theming layer or hooks such as hook_form_alter.
D. Code in it should be designed to fail gracefully when the module it is accomodating is repaired or changes.

II.  HOW MODULE FIXES WORK and HOW YOU MIGHT WRITE ONE

All individual module fixes are stored in the directory module_fixes directory and have a set of files such as MODULE.inc, MODULE.admin.inc, MODULE.documentation.inc, etc.

- MODULE.inc.  Required. This file is loaded if a module with the same name is enabled...it should have as little code as possible in it.  The MODULE.inc file should have any hooks in it.

- MODULE.admin.inc should have any administrative interfaces in it.  In particular, the functions accessible_fix_admin_module_fixes_MODULE and accessible_fix_admin_module_fixes_MODULE_submit are called to gather form elements for the configuration form.

If you would like to add to the admin form, its called "accessible_fix_admin_edit" so it can be altered with hook_form_FORM_ID_alter via a function named MODULE_form_accessible_fix_admin_edit_alter()

The major flaw with this architecture is that all the functions in MODULE.inc are prefixed with "accessible_fix" so a function in block.inc and search.inc could potentially be named the same and break Drupal.  Even worse the failure may not occur until the particular module is enabled causing major confusion about the cause of the problem.  Even so I think this architecture is more appropriate than another hook system for this module or an architecture of a bunch of sub modules.  SO SEARCH THE module_fixes directory before adding a duplicate function to the module_fixes folder.

III.  ADVOCACY

Module fixes should include a file called MODULE.help.inc that includes more information about the issue,
proposed patches, etc.  This will be available as a link from the admin form and at accessible_fix/help.
This type of information is best kept in public places like http://drupal.org/node/425494 where it can be
kept up to date and referenced from MODULE.help.inc.



