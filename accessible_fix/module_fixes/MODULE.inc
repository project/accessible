<?php
/**
 * @file
 * Template for accessible fixes for a given module
 * This file will be included if corresponding module is enabled
 */

/** general idea is to add hooks to module being fixed
 * here and constants and functions shared by
 * MODULE.inc and MODULE.admin.inc files
 *
 * DO MAKE SURE accessible_fix_* function names are not used by other module_fixes/*.inc files!!!!
 **/

define('ACCESSIBLE_FIX_MODULE_VARNAME_DEFAULT','my default value');

