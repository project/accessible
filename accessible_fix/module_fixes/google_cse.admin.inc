<?php

function accessible_fix_admin_module_fixes_google_cse($form) {

  $form['accessible_fix']['google_cse'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google CSE Additional Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['accessible_fix']['google_cse']['accessible_fix_google_cse_label'] = array(
    '#type' => 'textfield',
    '#title' => 'Label to use on Google CSE search input.',
    '#size' => 20,
    '#description' => t('If you are using Google CSE.  Defaults is: ' ) . ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT,
    '#default_value' =>  variable_get('accessible_fix_google_cse_label', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT)
  );

  $form['accessible_fix']['google_cse']['accessible_fix_google_cse_label_invisible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make Google CSE Label invisible'),
    '#description' => t('If you are using Google CSE, should label be invisible?'),
    '#default_value' =>  variable_get('accessible_fix_google_cse_label_invisible', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_INVISIBLE_DEFAULT)
  );
  
  return $form;
}
    

function accessible_fix_admin_module_fixes_google_cse_submit($form, &$form_state) {
 
  $values = $form_state['values'];

  if ($values['op'] == t('Reset') && $values['confirm'] == 1) {
    variable_set('accessible_fix_google_cse_label', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT);
    variable_set('accessible_fix_google_cse_label_invisible', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_INVISIBLE_DEFAULT);
  }
  else {
    variable_set('accessible_fix_google_cse_label',  $values['accessible_fix_google_cse_label']);
    variable_set('accessible_fix_google_cse_label_invisible',  $values['accessible_fix_google_cse_label_invisible']);
  }

}