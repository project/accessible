<?php



/**
 * Implements hook_form_FORM_ID_alter()
 */

function accessible_fix_form_block_add_block_form_alter(&$form, $form_state) {
  require_once('block.configure.form.inc');
  _accessible_fix_form_block_admin_configure_alter($form, $form_state);
}


function accessible_fix_form_block_admin_configure_alter(&$form, $form_state) {
  require_once('block.configure.form.inc');
  _accessible_fix_form_block_admin_configure_alter($form, $form_state);

}


/**
 * submit function is added in above form alters, so needs to be at module level so its available
 */
function accessible_fix_block_form_submit($form_id, $form_state) {
  require_once('block.configure.form.inc');
  _accessible_fix_block_form_submit($form_id, $form_state);
}


/**
 * implements hook_preprocess_block
 * to get block's accessibility configuration in block template
 * as variables
**/
function accessible_fix_preprocess_block(&$variables) {
  
  $storage_id = $variables['block']->module .'_'. $variables['block']->delta;
  $block_access = accessible_fix_get_data('block', $storage_id);

  $variables['block']->accessibility = new stdClass;  // need to make available even if empty for themeing

  if ($block_access = accessible_fix_get_data('block', $storage_id)) {
      // 1. make accessibility variables available to block for theming
  
    $aria_roles = array_keys($block_access['aria_roles']);
  
    foreach($block_access as $key => $value) {
      $variables['block']->accessible->{$key} = $value;
    }
  
    // get data into block array http://drupal.org/node/254940#html-class-variable
    if ($variables['block']->accessible->heading_invisible) {
      $variables['title_attributes_array']['class'][] = 'element-invisible';
    }
  
    $current_roles = (isset($variables['content_attributes_array']['role']) && $variables['content_attributes_array']['role']) ? $variables['content_attributes_array']['role'] : array();
    $variables['content_attributes_array']['role'] = array_unique(array_merge($current_roles, $aria_roles));
    // duplicates should be resolved in drupal_attributes() function with array_unique, but isn't yet. todo is patch
  }
  
  if (accessible_api_enabled_functionality('skip_nav')) {
    foreach ($variables['elements'] as $id => $element) {
      if ($id == '#block' && $element->module == 'menu') {
        $achor_name = 'end-menu-'. $element->bid;
        $variables['title_suffix'][] = array(
          '#markup' => l(
            t("skip !menu_name navigation", array('!menu_name' => $element->subject)),
            current_path(),
            array(
              'attributes' => array('class' => array('element-invisible')),
              'fragment' => $achor_name,
              )
            ),
          );
        $variables['content'] .= "<a id=\"$achor_name\"></a>";
   
      }
    }
  }
}