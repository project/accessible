<?php

define('ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_INVISIBLE_DEFAULT',   0);
define('ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT', "Google CSE Label!");
define('ACCESSIBLE_FIX_GOOGLE_CSE_BUTTON_TEXT_DEFAULT', "Google CSE Button Text!");

/**
 * Implements hook_form_FORM_ID_alter()
 */

function accessible_fix_form_google_cse_searchbox_form_alter(&$form, $form_state) {
 
  if (variable_get('accessible_fix_google_cse_label', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT)) {
    $form['query']['#title'] = variable_get('accessible_fix_google_cse_label', ACCESSIBLE_FIX_GOOGLE_CSE_LABEL_DEFAULT);
  }

}