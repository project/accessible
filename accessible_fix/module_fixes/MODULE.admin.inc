<?php

/**
 * @file
 * Template for accessible tweaks admin form for a given module
 */

/** replace MODULE with module being fixed for accessibility
 *    eg google_cse, search, etc.
 *
 *  replace VARNAME with the variable you are having the
 *    site admin set in the admin/settings/accessible/accessible_fix
 *    form
 */

function accessible_fix_admin_module_fixes_MODULE($form) {
 
 $MODULE_VARNAME =  variable_get('accessible_fix_MODULE_VARNAME', ACCESSIBLE_FIX_MODULE_VARNAME_DEFAULT);
 $form['accessible_fix_MODULE'] = array(
   '#type' => 'fieldset',
   '#title' => t('MODULE FRIENDLY NAME Additional Configuration'),
   '#collapsible' => TRUE,
   '#collapsed' => ($MODULE_VARNAME == ACCESSIBLE_FIX_MODULE_VARNAME_DEFAULT)  // leave collapsed if default value
   );
 
 // example of text field as configuration for MODULENAME
 $form['accessible_fix_MODULE']['accessible_fix_MODULE_VARNAME'] = array(
   '#type' => 'textfield',
   '#title' => 'Configuration Variable',
   '#size' => 20,
   '#description' => t('additional directions that no one will read so make the title good.'),
   '#default_value' => $MODULE_VARNAME
 );
 
 
 return $form;

}

function accessible_fix_admin_module_fixes_MODULE_submit($form, &$form_state) {
 
  $values = $form_state['values'];

  if ($values['op'] == t('Reset') && $values['confirm'] == 1) {
   variable_set('accessible_fix_MODULE_VARNAME', ACCESSIBLE_FIX_MODULE_VARNAME_DEFAULT);
  }
  else {
   variable_set('accessible_fix_MODULE_VARNAME',  $values['accessible_fix_MODULE_VARNAME']);
  }
}