<?php

$link = l('Accessible API Prefences', ACCESSIBLE_MENU_BASE_PATH .'/accessible_fix');
$invisible_block_headings =  <<<EOF
<div class="collapsible">
<h2>Invisible Block Headings</h2>
<p>Headings are important for accessibility (http://html.cita.illinois.edu/nav/heading/)
and search optization,
yet a visual inspection of navigation links or similar content makes a visible heading
overkill.  Invisible headings are a way to accomplish both.
</p>

<p>Invisible block headings involve three steps:</p>

<h3>1. Enable "Invisible Headings" in $link</h3>

<p>The Accessible API is for setting accessibility preferences.
The actual functionality of invisible headings for blocks is provided by
this module (Accessible Fix).</p>

<h3>2. Designate blocks as having invisible headings when editing them.</h3>
<p>When "Invisible Headings" are enabled, the block configuration form will have
a <code>Class this block's header tag as "element-invisible"</code> checkbox.
</p>
</div>

EOF;

?>