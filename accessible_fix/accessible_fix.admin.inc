<?php

/**
 * @file
 * The accessible module admininstration interface.
 * Includes module_fix/MODULE.admin.inc files for additional functionality
 */ 

/**
 * Admin form for Accessible Fix module.
 *
 * @param $form_state
 *   A form state array.
 * @param $sid
 *   A LDAP server ID.
 *
 * @return
 *   The form structure.
 */
function accessible_fix_admin_edit($form, &$form_state,$op) {

  if ($op == "edit" || $op == NULL) {
    
    $overriden_tpls =  variable_get('accessible_fix_overridden_tpls',array());

    $form['accessible_fix']['template_overrides'] = array(
        '#type' => 'fieldset',
        '#title' => t('Template Overrides'),
        '#collapsible' => TRUE,
        '#collapsed' => (! array_filter(array_values($overriden_tpls))),
    );

    $options = accessible_fix_available_tpls();
    $form['accessible_fix']['template_overrides']['overridden_tpls'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Templates to Override',
        '#size' => 10,
        '#options' => $options,
        '#description' => t('The following templates (.tpl files) will benefit
          from being overridden by the accessible_fix alternatives because of added
          accessibility markup.  This is a reasonable alternative to modifying the templates
          yourself, provided you have not made any changes in the theme files in the theme.'),
        '#default_value' => $overriden_tpls,
      );
    
    foreach(accessible_fix_viable_modules() as $module_name) {
      $function_name = "accessible_fix_admin_module_fixes_${module_name}";
      $path = drupal_get_path('module','accessible_fix') .'/module_fixes/' . $module_name . '.admin.inc';
      if (file_exists($path)) {
        require_once($path);
        if (function_exists($function_name)) {
          $form = call_user_func($function_name, $form);
        }
      }
    }
    
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );

    return $form;
  }
  else {
    drupal_goto(ACCESSIBLE_MENU_BASE_PATH .'/accessible_fix');
  }
}

// which tpls have override examples in examples/<theme>/<name>.tpl
function accessible_fix_available_tpls() {
  $themes = list_themes();
  $tpl_overrides = array();
  $template_overrides_dir = drupal_get_path('module','accessible_fix') .'/theme_overrides';
  foreach (accessible_fix_overrideable_themes() as $theme_dir) {
    foreach (scandir($template_overrides_dir .'/'. $theme_dir) as $tpl) {
      if (strpos($tpl,".tpl.php") > 0  && strpos($tpl, "accessible-fix") === 0) {
        $tpl = str_replace("accessible-fix-" ,"", $tpl);
        $tpl_stem = str_replace(".tpl.php" ,"", $tpl);
       //$tpl_stem = str_replace("accessible-fix-" ,"", $tpl);
        $tpl_overrides[$theme_dir ."_". $tpl_stem] = $theme_dir . '-' . $tpl;
      }
    }
  }
  return $tpl_overrides;
}

function accessible_fix_overrideable_themes() {

  $themes = list_themes();
  $tpl_overrides = array();
  $template_overrides_dir = drupal_get_path('module','accessible_fix') .'/theme_overrides';
  foreach (scandir($template_overrides_dir) as $theme_dir) {
    if (in_array($theme_dir, array_keys($themes)) && $themes[$theme_dir]->status == 1) {
      $tpl_overrides[] = $theme_dir;
    }
  }
  
  return $tpl_overrides;
      
}
/**
 * Submit hook for the admin settings form.
 */
function accessible_fix_admin_edit_submit($form, &$form_state) {
  
  $values = $form_state['values'];
  
  if ($values['op'] == t('Reset') && $values['confirm'] == 1) {
    }
  else {

    $values = $form_state['values'];
     variable_set('accessible_fix_overridden_tpls',  $values['overridden_tpls']);
  }
  
  foreach(accessible_fix_viable_modules() as $module_name) {
    $function_name = "accessible_fix_admin_module_fixes_${module_name}_submit";
    $path = drupal_get_path('module','accessible_fix') .'/module_fixes/' . $module_name . '.admin.inc';
    if (file_exists($path)) {
      require_once($path);
      if (function_exists($function_name)) {
        call_user_func($function_name, $form, $form_state);
      }
    }
  }
 
  if ($values['op'] == t('Reset') && $values['confirm'] == 1) {
     drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    drupal_set_message(t('The configuration options have been saved.'));
  } 

  $form_state['redirect'] = ACCESSIBLE_MENU_BASE_PATH .'/accessible_fix';
}

